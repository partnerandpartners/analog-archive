---
title: 'Gay Cable News Episode'
itemType: video
poster: https://archive.org/download/GayCableNews/GayCableNews.thumbs/GayCableNewsNetwork_001134.jpg
collections: []
keywords:
  - value: New York City
  - value: LGBTQ
  - value: 90s
  - value: AIDS
  - value: Activism
related: []
annotations:
  - body: >-
      (Clip #1)
    end: '0:02:10'
    start: '0:01:00'
  - body: >-
      (Clip #2)
    end: '0:12:55'
    start: '0:12:30'
  - body: >-
      (Clip #3)
    end: '0:18:40'
    start: '0:17:41'
  - body: >-
      (Clip #4)
    end: '0:12:55'
    start: '0:12:30'
  - body: >-
      (Clip #5)
    end: '0:12:55'
    start: '0:12:30'
  - body: >-
      (Clip #6)
    end: '0:12:55'
    start: '0:12:30'

mediaUrl: 'https://archive.org/download/GayCableNews/GayCableNewsNetwork.mp4'
---
Despite its low production values, cheap looking sets, and amateur presentation, the surface of the Bebashi tape's collection of short scenes belies a powerful representation of the sexual health issues surrounding African Americans and their communities of the period. Three different stories are presented (one in two parts) and share several actors. Addressed in these short vignettes are a myriad of difficult issues including — HIV/AIDS, domestic and child abuse, rape, drug use and rehabilitation, prostitution, gender norms, generational communication, and dying. A low-budget effort to explore these issues in a relatively brief and narrow frame seems like a recipe for almost certain failure but the boldness with which these controversial subjects are confronted -  including the raw authenticity of the dialogue, the complex nature of the circumstances presented, and the deeply-felt performances by the actors - combine to form something that is truly greater than the sum of its humble parts.

— *Adrian D. Cameron*
