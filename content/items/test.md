---
title: In C
itemType: audio
poster: >-
  /archive/media/cd_in-c-members-of-the-center-of-the-creative_terry-riley_itemimage.png
collections:
  - {}
keywords:
  - value: Terry Riley
annotations:
  - body: test one
    end: '00:00:05'
    start: '00:00:00'
  - body: test two
    end: '00:00:10'
    start: '00:00:05'
mediaUrl: >-
  https://ia801709.us.archive.org/11/items/TERRYRILEYInC25thAnniversaryConcert1995/TERRY%20RILEY%20In%20C%2025th%20Anniversary%20Concert%20%281995%29.mp3
length: '42:02'
---

