import { generate } from 'cooltilities'
import colors from './colors'

const styles = {
  ...generate(colors),
}

export { colors, styles }
